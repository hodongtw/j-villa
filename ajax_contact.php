<?
function stripslashes_( $arr )
{
	if (is_array($arr)){
	    $len = count($arr);
		for($i=0; $i<$len ; $i++)
			$arr[$i] = common_stripslashes(trim($arr[$i]));
	    return $arr;
	}
	else
		return common_stripslashes(trim($arr));
}

function common_stripslashes( $var )
{
    if (!get_magic_quotes_gpc()) {
        return $var;
    } else {		
        return stripslashes($var);
    }
}

function common_array_trim_stripslashes( $arr )
{
    $len = count($arr);
    for($i=0; $i<$len ; $i++)
        $arr[$i] = common_stripslashes(trim($arr[$i]));
    return $arr;
}

function IsEmail($email){
   if (eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,6}$", $email))
    return true;
  else
    return false;
}

foreach ($_GET as $k => $value) $$k = stripslashes_($value);
foreach ($_POST as $k => $value) $$k = stripslashes_($value);

$err = 1;
if (trim($name) == "") $msg = "請輸入姓名";
else if (trim($gender) == "") $msg = "選擇性別";
else if (trim($mobile) == "" || strlen($mobile) != 10) $msg = "請輸入手機號碼 10 碼";
else if (IsEmail($email) == false)  $msg = '請輸入 E-mail, 或 Email 格式錯誤';
else if (trim($agree) == "") $msg = "請勾選是否已閱讀並同意以下告知義務條款";
else {
	$err = 0;	
	$msg = "謝謝您";
	$to      = array('kan.w828530@gmail.com', 'michael_shao@hotmail.com','winzip119@gmail.com');
	$subject = '聯絡我們通知信';
	
	$message = '
		<html>
		<head>
		  <title>聯絡我們通知信</title>
		</head>
		<body>
		  <p>管理員您好, 以下是訪客留言通知</p>
		  <table>
		    <tr>
		      <th>姓名</th><td>'.$name.'</td>
		    </tr>
		   	<tr>
		      <th>性別</th><td>'.$gender.'</td>
		    </tr>
		   	<tr>
		      <th>手機</th><td>'.$mobile.'</td>
		    </tr>
		    <tr>
		      <th>電話</th><td>'.$telphone.'</td>
		    </tr>
		   	<tr> 
		      <th>電子信箱</th><td>'.$email.'</td>
		    </tr>
		   	<tr>
		      <th>聯絡地址</th><td>'.$address.'</td>
		    </tr>
		   	<tr>
		      <th>發送日期</th><td>'.date('Y-m-d H:i:s').'</td>
		    </tr>
		  </table>
		</body>
		</html>
		';
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'From: kan.w828530@msa.hinet.net' . "\r\n" .
	    'Reply-To: cforest0424@gmail.com' . "\r\n" .
	    'X-Mailer: PHP/' . phpversion();
	
	
	foreach($to as $k => $v){
		mail($v, $subject, $message, $headers);
	}
}

$task['err'] = $err;
$task['text'] = $msg;		
		
echo json_encode($task);
exit;
?>		