<?php defined('BASEPATH') OR exit('No direct script access allowed');

class faq_category_m extends Backend_Model {

    function __construct()
    {
    	$this->validate = 
		array(
			array(
			    'field' => 'name',
			    'label' => '名稱',
			    'rules' => 'required|max_length[50]',
			),		
			
		);
        // Call the Model constructor
        parent::__construct();
        
        $this->order = "id asc";
    }

}