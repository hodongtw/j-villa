<?php defined('BASEPATH') OR exit('No direct script access allowed');

class user_m extends MY_Model {

    function __construct()
    {
    	$this->validate = 
		array(
			array(
			    'field' => 'account',
			    'label' => '帳號',
			    'rules' => 'required',
			),
			array(
			    'field' => 'password',
			    'label' => '密碼',
			    'rules' => 'required',
			)
		);
        // Call the Model constructor
        parent::__construct();
    }

}