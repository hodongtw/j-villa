<?php defined('BASEPATH') OR exit('No direct script access allowed');

class faq_m extends Backend_Model {

    function __construct()
    {
    	$this->validate = 
		array(
			array(
			    'field' => 'category_id',
			    'label' => '分類',
			    'rules' => 'required',
			),	
			array(
			    'field' => 'name',
			    'label' => '問題',
			    'rules' => 'required|max_length[50]',
			),
			array(
			    'field' => 'answer',
			    'label' => '答案',
			    'rules' => 'required',
			),		
			
		);
        // Call the Model constructor
        parent::__construct();
        
        $this->order = "id asc";
    }

}