<?php 

class user extends Backend_Controller 
{
	public function __construct()
	{
		parent::__construct("backend");			

		$this->per_page = 20;
	}
	
	public function modify($id="", $skip_validation = FALSE, $redirect_root = FALSE)
	{
		parent::modify($id, $skip_validation, $redirect_root);
	}
}