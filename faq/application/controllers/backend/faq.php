<?php 

class faq extends Backend_Controller 
{
	public function __construct()
	{
		parent::__construct("backend");			
		
		$this->load->model("backend/faq_category_m");
		
		$this->data["categories"] = $this->get_html_select_data($this->faq_category_m->where("status","1")->get_all(),"id","name");

		$this->per_page = 20;
	}
}