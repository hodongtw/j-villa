<?php 

class login extends Backend_Info_Controller 
{
	public function __construct()
	{
		parent::__construct("backend");
		$this->load->model("backend/user_m");
	}
	
	public function index()
	{
		if($this->session->userdata('user'))
			redirect("backend/faq");
		
		if($_POST)
		{
			$where["account"] = $this->input->post('account');
			$admin = $this->user_m->get_by($where);
			if($admin){
				if($admin->password == $this->input->post('password')){
					$this->session->set_userdata('user',$admin);
					redirect("backend/faq");
				}
				else
					$this->data["msg"] = "帳號密碼錯誤";
			}
			else
				$this->data["msg"] = "帳號密碼錯誤";

		}

		$this->load->view('backend/login/index',$this->data);	
	}
	
	public function logout()
	{
		$this->session->unset_userdata('user');
		redirect("backend");
	}
}