<?php 

class faq extends Frontend_Controller 
{
	public function __construct()
	{
		parent::__construct();	
		$this->load->model("frontend/faq_category_m");
		$this->load->model("frontend/faq_m");
	}
	
	public function index($category_id = 0)
	{		
		$this->data["faq_categories"] = $this->faq_category_m->where("status",1)->get_all();
		
		foreach($this->data["faq_categories"] as $k=>$v):
			$this->data["faq_categories"][$k]->faqs = $this->faq_m->where("status",1)->where("category_id",$v->id)->get_all();
		endforeach;

		$this->load->view("frontend/faq/index",$this->data);
	}
	
}