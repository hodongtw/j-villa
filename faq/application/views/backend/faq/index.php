<!DOCTYPE html>
<html>
<? $this->load->view("backend/partials/head"); ?>
<body>
	<? $this->load->view("backend/partials/header"); ?>

	<!-- Main Container start -->
	<div class="dashboard-container">
		<div class="container">
			<? $this->load->view("backend/partials/nav"); ?>
			
			<!-- Sub Nav End -->
			<div class="sub-nav hidden-sm hidden-xs">
				<ul>
					<li><a href="" class="heading">常見問題</a></li>
					<li class="hidden-sm hidden-xs">
						<a href="<?=base_url("backend/faq_category")?>">分類管理</a>
					</li>
					<li class="hidden-sm hidden-xs">
						<a href="<?=base_url("backend/faq")?>" class="selected">常見問題管理</a>					
					</li>
				</ul>
			</div>
			<!-- Sub Nav End -->
			
			<!-- Dashboard Wrapper Start -->
			<div class="dashboard-wrapper-lg">
			
			<!-- Row Start -->
            <div class="row">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="widget">
						<div class="widget-header">
		                    <div class="title">
		                      常見問題管理
		                    </div>
		                    <span class="tools">
		                      <i class="fa"><a href="<?=site_url($site_root."/modify")?>">新增</a></i>
							   | 
		                      <i class="fa"><a href="#" class="dels" onclick="return false;">刪除</a></i>
		                    </span>
	                    </div>
	                    
	                    <div class="widget-body">
		                    <? if($err_msg):?>
				            <div class="alert alert-danger">
							 <strong><?=$err_msg?></strong>
							</div>
							<? endif;?>
							
							<? if($msg):?>
				            <div class="alert alert-success">
							 <strong><?=$msg?></strong>
							</div>
							<? endif;?>
							
		                    <? if($pagination):?><ul class="pagination"><?=$pagination?></ul><? endif;?>
		                    
		                    <form action="#" method="post" id="form">
							<table class="table table-responsive table-striped table-bordered table-hover no-margin">
								<thead>
			                        <tr>
			                          <th style="width:5%">
			                            <input type="checkbox" class="no-margin check_all" />
			                          </th>
			                          <th style="width:30%">
			                            分類
			                          </th>
			                          <th style="width:30%">
			                            標題
			                          </th>
			                          <th style="width:10%" class="hidden-xs">
			                            狀態
			                          </th>
			                          <th style="width:15%" class="hidden-xs">
			                            建立時間
			                          </th>
			                          <th style="width:10%">
			                            Actions
			                          </th>
			                        </tr>
		                        </thead>
		                        <tbody>
			                        <? foreach($items as $item):?>
			                        <tr data="<?=$item->id?>">
			                          <td>
			                            <input type="checkbox" name="ids[]" value="<?=$item->id?>" class="no-margin" />
			                          </td>
			                          <td><?=$categories[$item->category_id]?></td>
			                          <td field="name" class="input" data="<?=$item->name?>"><?=$item->name?></td>
			                          <td class="hidden-xs"><?=$item->status==1?"啟用":"停用"?></td>
			                          <td class="hidden-xs">
			                            <?=date('Y-m-d H:i:s', local_to_gmt($item->created_on)) ?>
			                          </td>
			                          <td>
			                            <div class="btn-group">
				                            <a href="<?=site_url($site_root.'/modify/'.$item->id)?>">編輯</a>
			                            </div>
			                          </td>
			                        </tr>
			                        <? endforeach;?>
			                    </tbody>
							</table>
		                    </form>
		                    
		                    <? if($pagination):?><ul class="pagination"><?=$pagination?></ul><? endif;?>
						</div>
					</div>				
                </div>
            </div>
            <!-- Row End -->
			
			</div>
			<!-- Dashboard Wrapper End -->

			<footer>
				<p></p>
			</footer>

		</div>
	</div>
	<!-- Main Container end -->

	<? $this->load->view("backend/partials/script"); ?>
	
</body>
</html>