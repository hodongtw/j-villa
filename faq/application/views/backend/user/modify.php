<!DOCTYPE html>
<html>
<? $this->load->view("backend/partials/head"); ?>
<body>
	<? $this->load->view("backend/partials/header"); ?>

	<!-- Main Container start -->
	<div class="dashboard-container">
		<div class="container">
			<? $this->load->view("backend/partials/nav"); ?>

			<!-- Dashboard Wrapper Start -->
			<div class="dashboard-wrapper-lg">
			
			<!-- Row Start -->
            <div class="row">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="widget">
						<div class="widget-header">
		                    <div class="title">
		                      系統帳號管理
		                    </div>
		                </div>
		                <div class="widget-body">
			            <? if($err_msg):?>
			            <div class="alert alert-danger">
						<? foreach($err_msg as $k=>$v):?>
						 <strong><?=$v?></strong> <br/>
						<? endforeach;?>
						</div>
						<? endif;?>
						
						<? if($msg):?>
			            <div class="alert alert-success">
						 <strong><?=$msg?></strong>
						</div>
						<? endif;?>
			                
	                    <form class="form-horizontal no-margin" method="post"> 
	                      
	                      <div class="form-group">
	                        <label for="title" class="col-sm-2 control-label"><span style="color:red"> * </span>帳號</label>
	                        <div class="col-sm-10">
	                          <input type="text" class="form-control" name="account" id="account" placeholder="帳號" value="<?=$item?$item->account:""?>">
	                        </div>
	                      </div>
	                      
	                      <div class="form-group">
	                        <label for="title" class="col-sm-2 control-label"><span style="color:red"> * </span>密碼</label>
	                        <div class="col-sm-10">
	                          <input type="text" class="form-control" name="password" id="password" placeholder="密碼" value="<?=$item?$item->password:""?>">
	                        </div>
	                      </div>
	                      
	                                           
	                      <button class="btn btn-primary btn-lg submit" type="button"><i class="icon-envelope"></i> 確認</button>
	                    </form>
		                </div>
					</div>				
                </div>
            </div>
            <!-- Row End -->
			
			</div>
			<!-- Dashboard Wrapper End -->

			<footer>
				<p></p>
			</footer>

		</div>
	</div>
	<!-- Main Container end -->
	<? $this->load->view("backend/partials/script"); ?>
</body>
</html>