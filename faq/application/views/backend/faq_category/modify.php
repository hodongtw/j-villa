<!DOCTYPE html>
<html>
<? $this->load->view("backend/partials/head"); ?>
<body>
	<? $this->load->view("backend/partials/header"); ?>

	<!-- Main Container start -->
	<div class="dashboard-container">
		<div class="container">
			<? $this->load->view("backend/partials/nav"); ?>
			
			<!-- Sub Nav End -->
			<div class="sub-nav hidden-sm hidden-xs">
				<ul>
					<li><a href="" class="heading">常見問題</a></li>
					<li class="hidden-sm hidden-xs">
						<a href="<?=base_url("backend/faq_category")?>" class="selected" >分類管理</a>
					</li>
					<li class="hidden-sm hidden-xs">
						<a href="<?=base_url("backend/faq")?>" >常見問題管理</a>					
					</li>
				</ul>
			</div>
			<!-- Sub Nav End -->
			
			<!-- Dashboard Wrapper Start -->
			<div class="dashboard-wrapper-lg">
			
			
			<!-- Row Start -->
            <div class="row">
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="widget">
						<div class="widget-header">
		                    <div class="title">
		                      分類管理
		                    </div>
		                </div>
		                <div class="widget-body">
			            <? if($err_msg):?>
			            <div class="alert alert-danger">
						<? foreach($err_msg as $k=>$v):?>
						 <strong><?=$v?></strong> <br/>
						<? endforeach;?>
						</div>
						<? endif;?>
			                
	                    <form class="form-horizontal no-margin" method="post"> 
	                      
	                      <div class="form-group">
	                        <label for="name" class="col-sm-2 control-label"><span style="color:red"> * </span>名稱</label>
	                        <div class="col-sm-10">
	                          <input type="text" class="form-control" name="name" id="name" placeholder="名稱" value="<?=$item?$item->name:""?>">
	                        </div>
	                      </div>
	                     
	                      <div class="form-group">
	                        <label for="status" class="col-sm-2 control-label"><span style="color:red"> * </span>狀態</label>
	                        <div class="col-sm-10">
		                       <select name="status" id="status" class="form-control" >
			                    <option value="0" <?=$item?$item->status==0?"selected":"":"selected"?>>停用</option>
								<option value="1" <?=$item?$item->status==1?"selected":"":""?>>啟用</option>
							   </select>
	                        </div>
	                      </div>


	                      <button class="btn btn-primary btn-lg submit" type="button"><i class="icon-envelope"></i> 確認</button>
						  <button class="btn btn-default btn-lg" type="button" onclick="location.href='<?=site_url($site_root)?>'">取消</button>
	                    </form>
		                </div>
					</div>				
                </div>
            </div>
            <!-- Row End -->
			
			</div>
			<!-- Dashboard Wrapper End -->

			<footer>
				<p></p>
			</footer>

		</div>
	</div>
	<!-- Main Container end -->
	<? $this->load->view("backend/partials/script"); ?>
</body>
</html>