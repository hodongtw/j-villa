	<input type="hidden" id="site_root" value="<?=site_url($site_root)?>"/>
	<input type="hidden" id="img_src" value="<?=$img_src?>"/>
	<script src="<?=base_url('contents/backend/js/jquery.js')?>"></script>
	<script src="<?=base_url('contents/backend/js/bootstrap.min.js')?>"></script>
	<script src="<?=base_url('contents/backend/js/wysi/wysihtml5-0.3.0.min.js')?>"></script>
	<script src="<?=base_url('contents/backend/js/wysi/bootstrap3-wysihtml5.js')?>"></script>
	<script src="<?=base_url('contents/backend/js/jquery.scrollUp.js')?>"></script>

	<!-- Color Picker -->
    <script src="<?=base_url('contents/backend/js/color-picker/jquery.minicolors.js')?>"></script>
    
	<!-- bootstrap-fileinput JS -->
	<script src="<?=base_url('vendor/kartik-v/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js')?>"></script>
	<script src="<?=base_url('vendor/kartik-v/bootstrap-fileinput/js/fileinput.min.js')?>"></script>
	<script src="<?=base_url('vendor/kartik-v/bootstrap-fileinput/js/fileinput_locale_zh-TW.js')?>"></script>
	
	<!-- Custom JS -->
	<script type="text/javascript" src="<?=site_url('contents/backend/js/ckeditor/ckeditor.js')?>"></script>
	<script type="text/javascript" src="<?=site_url('contents/backend/js/ckfinder/ckfinder.js')?>"></script>
	
	<script src="<?=base_url('contents/backend/js/menu.js')?>"></script>
	<script src="<?=base_url('contents/backend/js/custom.js')?>"></script>
	
	<script type="text/javascript">
		//ScrollUp
		$(function () {
			$.scrollUp({
			scrollName: 'scrollUp', // Element ID
			topDistance: '300', // Distance from top before showing element (px)
			topSpeed: 300, // Speed back to top (ms)
			animation: 'fade', // Fade, slide, none
			animationInSpeed: 400, // Animation in speed (ms)
			animationOutSpeed: 400, // Animation out speed (ms)
			scrollText: 'Top', // Text for element
			activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
			});
		});
	</script>