			<!-- Top Nav Start -->
			<div id='cssmenu'>
				<ul>
					
					<li <?=in_array($this->uri->segments[2], array("faq_category","faq"))?"class='active'":""?>>
						<a href="#"><i class="fa fa-question"></i>常見問題</a>
						<ul>
							<li><a href='<?=site_url("backend/faq_category")?>'>分類管理</a></li>
							<li><a href='<?=site_url("backend/faq")?>'>常見問題管理</a></li>
						</ul>
					</li>
					
					<li <?=in_array($this->uri->segments[2], array("user"))?"class='active'":""?>>
						<a href="<?=site_url("backend/user/modify/1")?>"><i class="fa fa-user"></i>系統帳號管理</a>
					</li>
				</ul>
			</div>
			<!-- Top Nav End -->