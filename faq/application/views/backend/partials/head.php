<head>
	<title><?=$title?></title>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<!-- 	<link rel="shortcut icon" href="<?=base_url('contents/backend/img/favicon.ico')?>"> -->
	
	<link href="<?=base_url('contents/backend/css/bootstrap.min.css')?>" rel="stylesheet">
	<link href="<?=base_url('contents/backend/css/wysi/bootstrap-wysihtml5.css')?>" rel="stylesheet">
	<link href="<?=base_url('contents/backend/css/new.css')?>" rel="stylesheet"> 
	<link href="<?=base_url('contents/backend/css/charts-graphs.css')?>" rel="stylesheet">
	<!-- Datepicker CSS -->
	<link rel="stylesheet" type="text/css" href="<?=base_url('contents/backend/css/datepicker.css')?>">
	
	<!-- Color Picker -->
	<link rel="stylesheet" href="<?=base_url('contents/backend/css/color-picker/jquery.minicolors.css')?>">
	
	<link href="<?=base_url('contents/backend/fonts/font-awesome.min.css')?>" rel="stylesheet">
	<link href="<?=base_url('vendor/kartik-v/bootstrap-fileinput/css/fileinput.min.css')?>" rel="stylesheet">

	<!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="<?=base_url('contents/backend/js/html5shiv.js')?>"></script>
    <script src="<?=base_url('contents/backend/js/respond.min.js')?>"></script>
	<![endif]-->
</head>