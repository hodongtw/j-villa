	<!-- Header Start -->
	<header>
		<a href="<?=site_url()?>" class="logo">
			<img src="<?=base_url('contents/backend/img/logo.png')?>" alt="Logo"/>
		</a>
		<div class="pull-right">
			<ul id="mini-nav" class="clearfix">
				<li class="list-box" >
					<a href="<?=site_url("backend/login/logout")?>">
						<span class="text-white">Logout</span>
					</a>
				</li>
			</ul>
		</div>
	</header>
	<!-- Header End -->