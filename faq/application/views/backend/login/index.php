
<!DOCTYPE html>
<html>
  <head>
    <title><?=$title?></title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<!--     <link rel="shortcut icon" href="<?=base_url('faq/contents/backend/img/favicon.ico')?>"> -->
    
    <link href="<?=base_url('contents/backend/css/bootstrap.min.css')?>" rel="stylesheet">

    <link href="<?=base_url('contents/backend/css/new.css')?>" rel="stylesheet">
    <!-- Important. For Theming change primary-color variable in main.css  -->

    <link href="<?=base_url('contents/backend/fonts/font-awesome.min.css')?>" rel="stylesheet">

    <!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=base_url('contents/backend/js/html5shiv.js')?>"></script>
      <script src="<?=base_url('contents/backend/js/respond.min.js')?>"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Main Container start -->
    <div class="dashboard-container">

      <div class="container">

        <!-- Row Start -->
        <div class="row">
          <div class="col-lg-4 col-md-4 col-md-offset-4">
            <div class="sign-in-container">
              <form class="login-wrapper" method="post">
                <div class="header">
                  <div class="row">
                    <div class="col-md-12 col-lg-12">
                      <h3>Login<img src="<?=base_url('contents/backend/img/logo1.png')?>" alt="Logo" class="pull-right"></h3>
                      <p>Fill out the form below to login.</p>
                    </div>
                  </div>
                </div>
                <div class="content">
                  <? if($msg):?>
                    <div class="alert alert-danger">
                      <strong>錯誤!</strong> <?=$msg?>
          				  </div>
                  <? endif;?>
                  <div class="form-group">
                    <label for="userName">User Name</label>
                    <input type="text" class="form-control" name="account" id="userName" placeholder="User Name">
                  </div>
                  <div class="form-group">
                    <label for="Password1">Password</label>
                    <input type="password" class="form-control" name="password" id="Password1" placeholder="Password">
                  </div>
                </div>
                <div class="actions">
                  <input class="btn btn-danger" name="Login" type="submit" value="Login">
<!--                   <a class="link" href="#">Forgot Password?</a> -->
                  <div class="clearfix"></div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- Row End -->
        
      </div>
    </div>
    <!-- Main Container end -->

    <script src="<?=base_url('contents/backend/js/jquery.js')?>"></script>
    <script src="<?=base_url('contents/backend/js/bootstrap.min.js')?>"></script>

  </body>
</html>