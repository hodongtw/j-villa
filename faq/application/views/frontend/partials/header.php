<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="index, follow" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=480" />
<title>Jvilla悅榕莊園</title>
<meta property="og:title" lang="zh-Hant-TW"  content="Jvilla悅榕莊園" />
<meta property="og:site_name" lang="zh-Hant-TW" content="Jvilla悅榕莊園"/>
<meta property="og:url" content="http://" />
<meta property="og:description" lang="zh-Hant-TW"  content="國際新軸心BKK1使館特區" />
<meta property="og:image" content="img/kvh.jpg" />
<meta property="og:type"  content="website" />
<link href="<?=base_url("contents/frontend")?>/css/layout.css?<?=time()?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url("contents/frontend")?>/css/animations.css" rel="stylesheet" type="text/css" />

<link href="<?=base_url("contents/frontend")?>/css/qa.css?<?=time()?>" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?=base_url("contents/frontend")?>/css/vision_a.css" />
<script type="text/javascript" src="<?=base_url("contents/frontend")?>/js/jquery.js"></script>
<script type="text/javascript" src="<?=base_url("contents/frontend")?>/js/menu.js"></script>
<script type="text/javascript" src="<?=base_url("contents/frontend")?>/js/respond.js"></script>
</head>

<body>
<img src="<?=base_url("contents/frontend")?>/img/kvh.jpg" style=" display:none" /> 


<!--header---------------------------------------------------------------------------------------------------------->

<header>
  <table width="100%" style="max-width:950px;" border="0" align="center" cellpadding="0" cellspacing="0" height="75">
    <tr>
      <td align="center">
	      <a href="<?=base_url("")?>../">
		      <img src="<?=base_url("contents/frontend")?>/img/menu/logo.png" width="100%" style="max-width:215px" class="logo expandOpen kvw"/>
		      <img src="<?=base_url("contents/frontend")?>/img/menu/logo.png" width="100%" style="max-width:151px" class="logo expandOpen kvh"/>
		  </a>
	  </td>
      <td width="5%" class="kvw"></td>
      <td align="center" class="kvw"><ul class="menuitem" id="menu">
          <li class="slideDown">
          	<a href="<?=base_url("")?>../vision_a.html">
	          	<img src="<?=base_url("contents/frontend")?>/img/menu/item0.png" alt="" />
	        </a>
	      </li>
          <li class="slideDown">
          	<a href="<?=base_url("")?>../phnom_penh_a.html">
	          	<img src="<?=base_url("contents/frontend")?>/img/menu/item1.png" alt="" />
	        </a>
	      </li>
          <li class="slideDown">
          	<a href="<?=base_url("")?>../building_a.html">
	          	<img src="<?=base_url("contents/frontend")?>/img/menu/item2.png" alt="" />
	        </a>
	      </li>
          <li class="slideDown">
          	<a href="<?=base_url("")?>../floor_plan.html">
	          	<img src="<?=base_url("contents/frontend")?>/img/menu/item3.png" alt="" />
	        </a>
	      </li>
          <li class="slideDown">
          	<a href="<?=base_url("")?>../team.html">
	          	<img src="<?=base_url("contents/frontend")?>/img/menu/item4.png" alt="" />
	        </a>
	      </li>
          <li class="slideDown">
          	<a href="<?=base_url("")?>../contact.html">
	          	<img src="<?=base_url("contents/frontend")?>/img/menu/item5.png" alt="" />
	        </a>
	      </li>
        </ul></td>
    </tr>
  </table>
 <div id="enbtn" class="kvw">
	 <a href="<?=base_url("")?>../en/index.html"></a>
 </div>
 <div id="qabtn" class="kvw">
	 <a href="<?=base_url("faq")?>">Q&A</a>
 </div>
 <div id="phoneEN" class="kvh">
	 <a href="<?=base_url("")?>../en/index.html"></a>
 </div>
 <div id="phoneMENU" class="kvh">
	 <a href="#" id="btnon">
		 <img src="<?=base_url("contents/frontend")?>/img/menu/phonemeun.png"/>
     </a>
     <a href="#" name="btnoff" id="btnoff">
	  	 <img src="<?=base_url("contents/frontend")?>/img/menu/phonemeun.png"/>
	 </a>
 </div>
</header>
<ul class="popmenu kvh" id="onoff">
  <li> <a href="<?=base_url("")?>../vision_a.html">勝選柬國</a></li>
  <li> <a href="<?=base_url("")?>../phnom_penh_a.html">城市核心</a></li>
  <li> <a href="<?=base_url("")?>../building_a.html">綠動建築</a></li>
  <li> <a href="<?=base_url("")?>../floor_plan.html">房型配置</a></li>
  <li> <a href="<?=base_url("")?>../team.html">頂尖團隊</a></li>
  <li> <a href="<?=base_url("")?>../contact.html">聯絡我們</a></li>
  <li> <a href="<?=base_url("faq")?>">Q&A</a></li>
</ul>

<!--內容載入---------------------------------------------------------------------------------------------------->