<? $this->load->view("frontend/partials/header"); ?>

<div class="main2"> 
  
  <!--抬頭選單-->
  <table width="100%" height="100" border="0" align="center" cellpadding="0" cellspacing="10" bgcolor="#FFFFFF">
    <tr>
      <td align="center" valign="middle"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:1000px">
          <tr>
            <td width="300">
	          <table width="100" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td>
	                  <img src="<?=base_url("contents/frontend")?>/img/qa/title.png" name="pa00" class="kvw" id="pa" style="max-width:300px"/>
	                  <img src="<?=base_url("contents/frontend")?>/img/vision/title01.png" name="pa00" width="180" class="kvh" id="pa00" style="max-width:300px"/>
	              </td>
                </tr>
                <tr>
                  <td height="10"  class="kvh"></td>
                </tr>
                <tr>
                  <td  class="kvh" height="50">
	                  <img src="<?=base_url("contents/frontend")?>/img/vision/title02.png" width="362" class="kvh" style="position:absolute; margin-top:-20px" />
	              </td>
                </tr>
              </table>
            </td>
            <td width="20">&nbsp;</td>
            <td valign="bottom">&nbsp;</td>
            <td valign="top"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <!--內容開始-->
	<!--pa01-->
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#e9e9e9">
	  <tr>
	    <td align="center">
	    
	    
	    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="max-width:980px">
	  <tr>
	    <td height="40"></td>
	  </tr>
	  <tr>
	    <td align="center" class="qatitle">
		    <a href="#" id="00" class="faq_cate qatitlein">All</a> | 
		    
		    <? foreach($faq_categories as $k=>$v): ?>
		    <a href="javascript:;" class="faq_cate qustions"><?=$v?$v->name:"";?></a> | 
		    <? endforeach; ?>

		</td>
	  </tr>
	  <tr>
	    <td align="center" height="20"></td>
	  </tr>
	  <tr>
	     <td height="40">
     
<!-- QA    -->
      <div class="menu">
        <ul class="allbtn">
            
            <? foreach($faq_categories as $k=>$v): ?>
            
            <? foreach($v->faqs as $kk=>$vv):?>
            <li class="btncolor ans_<?=$k?>">
                <a href="#"><img src="<?=base_url("contents/frontend")?>/images/arrow_u.png" class="arr" /><?=$vv->name?></a>
                <ul class="submenu">
                    <li><?=nl2br($vv->answer)?></li>
                </ul>
            </li>
             <? endforeach; ?>
            
            <? endforeach; ?>
            
        </ul>
    </div>   
     
     
     
     
     
     
     
     
     
     </td>
  </tr>
  <tr>
    <td height="60">&nbsp;</td>
  </tr>
    </table>

    
    
    </td>
  </tr>
</table>


</div>

<? $this->load->view("frontend/partials/footer"); ?>
