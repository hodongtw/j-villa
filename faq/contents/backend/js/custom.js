$(function () {
	// Check / uncheck all checkboxes
    $('.check_all').click(function () {
        $(this).parents('form').find('input:checkbox').prop('checked', $(this).is(':checked'));
    });
    
    $(".submit").click(function(){
	    $(this).parents('form').submit();
    });
    	
	//wysihtml5
    $('.textarea').wysihtml5();
    
    //設定ckfinder
	$(".ckobj").each(function(){
		CKEDITOR.replace( $(this).attr("name"),
		{
		    filebrowserBrowseUrl : '/contents/backend/js/ckfinder/ckfinder.html',
		    filebrowserImageBrowseUrl : '/contents/backend/js/ckfinder/ckfinder.html?Type=Images',
		    filebrowserFlashBrowseUrl : '/contents/backend/js/ckfinder/ckfinder.html?Type=Flash',                     
		    filebrowserUploadUrl:'/contents/backend/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',       
		    filebrowserImageUploadUrl:'/contents/backend/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		    filebrowserFlashUploadUrl :'/contents/backend/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
	    });
	})

	
	//設定色票外掛
	$('.colorpick').each( function() {
      $(this).minicolors({
        control: $(this).attr('data-control') || 'hue',
        defaultValue: $(this).attr('data-defaultValue') || '',
        inline: $(this).attr('data-inline') === 'true',
        letterCase: $(this).attr('data-letterCase') || 'lowercase',
        opacity: $(this).attr('data-opacity'),
        position: $(this).attr('data-position') || 'bottom left',
        change: function(hex, opacity) {
          if( !hex ) return;
          if( opacity ) hex += ', ' + opacity;
          try {
            console.log(hex);
          } catch(e) {}
        },
        theme: 'bootstrap'
      }); 
    });
    
    // Reg fileupload button
    reg_ajaxfileupload();
    
    // Reg Form Input
    reg_input();
    
    reg_copys();
    reg_dels();
});

function reg_ajaxfileupload()
{
	$(".ajaxfileuplod").each(function() { 
		set_fileupload($(this));
	});
}

function set_fileupload(obj)
{
	
	var init_files = [];
	var initialPreview = [];
	var initialPreviewConfig = [];
	
	if(obj.val())
		init_files = JSON.parse(obj.val());
		
	
	
	init_files.forEach(function(file,i) {
    	initialPreview.push("<img style='height:160px' src='"+$("#img_src").val()+"/"+obj.attr("folder")+"/"+file + "'>");
    	var preview = new Object;
    	preview.caption = file;
    	preview.width = "120px";
    	preview.url = $("#site_root").val() + "/filedelete";
    	preview.key = file;
    	preview.extra = {folder:obj.attr("folder")};
    	initialPreviewConfig.push(preview);
	});
	
	console.log(initialPreviewConfig);
	
	var setting = {
		language: "zh-TW",
	    uploadUrl: $("#site_root").val() + "/fileupload",
	    allowedFileExtensions: ["jpg", "png", "gif"],
		uploadAsync: true,
		overwriteInitial: true,
		validateInitialCount: true,
		showUpload: false, // hide upload button
		showRemove: false, // hide remove button
		initialPreview: initialPreview,
	    initialPreviewConfig: initialPreviewConfig,
	    uploadExtraData: {
	        folder: obj.attr("folder"),
	    }
	};
	
	
	f_html = $('<input type="file" accept="image/*" />');
	
	//是否為多檔上傳
	if(obj.attr("multiple"))
	{
		f_html.prop("multiple" , true);
		
		if(obj.attr("maxFileCount"))
			setting["maxFileCount"] = parseInt(obj.attr("maxFileCount"));
	}
	else
		setting["maxFileCount"] = 1;
		
	obj.after(f_html);
	
	var f_obj = obj.next();
	
	f_obj.fileinput(setting)
	.on("filebatchselected", function(event, files) {
	    // trigger upload method immediately after files are selected
	    f_obj.fileinput("upload");
	})
	.on('filedeleted', function(event, key) {
		
		new_init_files = [];
		
    	init_files.forEach(function(file,i) {
	    	if(file != key)
		    	new_init_files.push(file);
		});
		
		init_files = new_init_files;

		obj.val(JSON.stringify(init_files));
	})
	.on('fileuploaded', function(event, data, previewId, index) {
	    var files = data.response.initialPreviewConfig;
	    files.forEach(function(file) {
		    init_files.push(file.key);
		});
		obj.val(JSON.stringify(init_files));
	});
	;
}


function reg_input()
{
	$(".input").click(function(){ 

		$(this).html('<input type="text" value="' + $(this).html() + '" style="width:' + (($(this).html().length+1) * 10) + 'px"/>');
		td = $(this);
		tr = td.parent();
		input = td.children("input").eq(0);
		input.focus();
		input.select();
		$(".input").unbind("click");
		input.blur(function(){
			if($(this).val() != td.attr("data"))
			{
				td.attr("data",$(this).val());
				ajaxupdate_field(tr.attr("data"),td.attr("field"),td.attr("data"));
			}
			td.html($(this).val());
			reg_input();
		});
	})			
}

function ajaxupdate_field(id, field_name, value)
{
	$.ajax({
		type: "POST",
		url: $("#site_root").val() + "/update_field/" + id,
		data: field_name + "=" + value,
		dataType: 'json',
		success: function(msg){
			if(msg)
			{
				alert(eval("msg."+field_name));
				location.reload();
			}
        }
	});
}


//註冊複製按鈕
function reg_copys()
{
	$(".copys").click(function(){
		if($("input:checked").length=="0")
		{
			alert("尚未選取項目");
		}
		else
		{
			$("#form").attr("action",$("#site_root").val()+"/copys");
			$("#form").submit();
		}
	})
}

//註冊刪除按鈕
function reg_dels()
{
	$(".dels").click(function(){
		if($( "input:checked" ).length=="0")
		{
			alert("尚未選取項目");
		}
		else
		{
			if(confirm("確定要刪除?"))
			{
				$("#form").attr("action",$("#site_root").val()+"/deletes");
				$("#form").submit();
			}
		}
	})
	
	$("a",$(".delete")).click(function(){
		if($(this).html() == "刪除")
		{
			if(confirm("確定要刪除?"))
			{
				location.href = $("#site_root").val() + "/delete/" + $(this).parent().parent().attr("data");
			}
			return false;
		}
	})
}