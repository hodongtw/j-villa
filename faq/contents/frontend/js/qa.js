
    $(function() {

        var $subMenus = $(".submenu > li");
        $(".menu > ul > li > a").click(function() {
            var $this = $(this),
                $ul = $this.next('ul');

            var $sibu = $('li.btncolor ul:visible');
            if ($sibu.length > 0) {
                $sibu.each(function(i, ele) {
                    var $ele = $(ele),
                        $parent = $ele.parent(),
                        $a = $parent.find('>a');
                    $a.html($a.html().replace("_d.png", "_u.png")).next('ul').slideUp();
                });
            }
            if ($ul.length > 0) {
                if ($ul.is(":visible")) {
                    $this.html($this.html().replace("_d.png", "_u.png")).next().slideUp();
                } else {
                    $subMenus.slideDown();
                    $this.parent().siblings('li').find('>a img').attr('src', function(i, v) {
                        return this.src.replace("_d.png", "_u.png");
                    }).end().find('ul').slideUp();
                    $this.html($this.html().replace("_u.png", "_d.png")).next().slideDown();
                }

                return false;
            }
        });

        $subMenus.find('a').click(function() {
            var $this = $(this),
                $ul = $this.next('ul');
            if ($ul.length > 0) {
                if ($ul.is(":visible")) {
                    $this.html($this.html().replace("_d.png", "_u.png")).next().slideUp();
                } else {
                    $this.parent().siblings('li').find('>a img').attr('src', function(i, v) {
                        return this.src.replace("_d.png", "_u.png");
                    }).end().find('ul').slideUp();
                    $this.html($this.html().replace("_u.png", "_d.png")).next().slideDown();
                }
                return false;
            }
        });

        $("a").focus(function() {
            $(this).blur();
        });
    });