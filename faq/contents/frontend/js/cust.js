$(document).ready(function() {
	
	$('#contactForm').submit(function(){
		$.ajax({
			type: "POST",
			url: "/ajax_contact.php",
			dataType: 'json',
			data: $('#contactForm').serialize(),
			error: function(){
				
				alert("Ajax request error");
			},
			success: function(response){
				if (response.err == 0)
				{
					alert(response.text);
					location.href= 'index.html';
				}
				else
				{		
					alert(response.text);
				}						
			}
		});
		return false;
	});
});